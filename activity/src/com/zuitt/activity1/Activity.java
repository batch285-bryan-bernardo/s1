package com.zuitt.activity1;
import java.util.Scanner;
public class Activity {
    public static void main(String[] args){

        Scanner myObj = new Scanner(System.in);

        System.out.println("Enter First Name: ");
        String firstName = myObj.nextLine();

        System.out.println("Enter Last Name: ");
        String lastName = myObj.nextLine();

        System.out.println("First Subject Grade: ");
        Double firstGrade = myObj.nextDouble();

        System.out.println("Second Subject Grade: ");
        Double secondGrade = myObj.nextDouble();

        System.out.println("Third Subject Grade: ");
        Double thirdGrade = myObj.nextDouble();

        Double averageGrade = (firstGrade + secondGrade + thirdGrade)/3;

        System.out.println("Hello! " + firstName + " " + lastName +".");
        System.out.println("Your grade average is "  + averageGrade);
    }

}
