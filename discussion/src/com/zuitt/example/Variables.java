//Package in Java is used to group related classes
//Think of it as a folder in a file directory
    // packages are divide into 2 categories.
    //1. Built-in Packages (packages from Java API)
    //2. User-define Packages (create your own package)

//reverse domain name notation
package com.zuitt.example;

public class Variables {
//    for the meantime we will run within the class file
    public static  void main (String[] args){
        // Naming conventions
        // The terminology used for variable names is "identifier"
        //Syntax: dataType identifier;

        // Variable
        int age;
        char middleName;

        int x;
        int y = 1;


        // Initialization after declaration
        x = 1;

        // Changing the values
        y = 2;

        System.out.println("The value of y is " + y + " and the value of x is " + x);

        // Primitive DataTypes
        // Pre-defined within the Java programming language which is used for single-valued variables with limited capabilities.

        // int - A whole number values
        int wholeNumber = 100;
        System.out.println(wholeNumber);

        // long
        // L is added to the end of the long number to be recognized
        long worldPopulation = 89512312738123L;

        // float
        // add f at the end of a float
        float piFloat = 3.14159265359f;
        System.out.println(piFloat);

        // double - floating point values
        double piDouble = 3.14159265359;
        System.out.println(piDouble);

        // char - single characters
        char letter = 'c';
        System.out.println(letter);

        // boolean
        boolean isLove = true;
        boolean isTaken = false;
        System.out.println(isLove);
        System.out.println(isTaken);

        // constant
        // final - keyword
        // common practice - CAPITAL LETTERS for readability
        final int PRINCIPAL = 3000;
        System.out.println(PRINCIPAL);
        //PRINCIPAL = 4000;

        // non-primitive data types
        // can store multiple values
        // aka as reference data types - refer to instances or objects
        // do not directly store the value of a variable, but rather remembers the reference to the variable

        // String
        // stores a sequence or array of characters

        String userName = "CardoD";
        System.out.println(userName);
        // Sample String Methods
        int stringLength = userName.length();
        System.out.println(stringLength);
    }

}
