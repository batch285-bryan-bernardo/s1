// Main Class
    // Entry point of our java program
    // Main class has 1 method inside, the main method
        // Main method is to run our code
        // It is where running and execution happens.
    // Public/public - is an access modifier which simply tells the app which classes have access to method/attributes
    // Static/static - is a keyword associated with a method/property that is related in a class.
        // static allows a method to be invoked without instantiating a class.
    // Void/void - is a keyword used to specify a method that doesn't return anything. In Java, we have to declare the data type of the method's return.
public class Main {
    public static void main(String[] args) {

        System.out.println("Hello world!");

    }
}